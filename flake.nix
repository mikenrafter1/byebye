{
  outputs = { self, nixpkgs, flake-utils }: # list out the dependencies
    let
      name = "byebye";
      set  = "haskellPackages";
      overlay = self: super: { # a pattern of bring build artifacts to pkgs
        ${set} = super.${set}.override {
          overrides = hself: hsuper: {
            ${name} = hself.callCabal2nix name
              (self.nix-gitignore.gitignoreSourcePure [
                ./.gitignore
                "flake.nix"
              ] ./.) { };
          };
        };
        ${name} =
          self.haskell.lib.justStaticExecutables self.${set}.${name};
      };
    in {
      inherit overlay;
    } // flake-utils.lib.eachDefaultSystem (system: # leverage flake-utils
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ overlay ];
        };
      in {
        defaultPackage = pkgs.${name};
        devShell = pkgs.${set}.shellFor { # development environment
          packages = p: [ p.${name} ];
          buildInputs = with pkgs.${set}; [
            cabal-install
            ghcid
            ormolu
            hlint
            pkgs.nixpkgs-fmt
          ];
        };
      });
}

