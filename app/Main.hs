{-# LANGUAGE OverloadedLabels  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ImplicitParams #-}

module Main where

--{- imports
import Data.GI.Base
import GI.Gtk hiding (main)
import qualified GI.Gtk as Gtk
import System.Directory
import System.Posix.User
import System.Process

import GHC.Word
import GHC.Int
import Data.Text (Text(..), pack, unpack)

import Control.Concurrent (threadDelay)
--}

--{- user variable types
padding, width, lineHeight :: Int32
choices :: [(String, IO ())]
prefix, suffix :: String
confirm :: Bool
--}

--{- menu choices
choices  =
  [ ("cancel",    pure ()                          )
  , ("logout",    cmd "killall xmonad-x86_64-linux")
  , ("reboot",    cmd "reboot"                     )
  , ("shutdown",  cmd "shutdown -h now"            )
  , ("suspend",   cmd "systemctl suspend"          )
  , ("hibernate", cmd "systemctl hibernate"        )
  , ("lock",      cmd "loginctl lock-session"      )
  ] where cmd = callCommand
--}

--{- user defined locations
-- relative to your $HOME directory
prefix = "/doc/code/cloned/haskell/byebye/img/"
suffix = ".png"
--}


-- inter-item and border-padding
padding     = 10
-- icon width
width       = 128
-- fontSize/lineHeight
lineHeight  = 16
-- use confirm/don't confirm behavior
confirm     = True


-- [i|i|i] - 3 icons, 4 pads, 2 outer & 2 inner
calc'wide :: Int32 -> Int32
calc'wide    len = fromIntegral $ padding*(len+1) + width*len

-- assuming font size of 16...
-- [i|l]   - 1 img, 1 label, 3 pads, 1 inner, 2 outer
calc'tall :: Int32 -> Int32
calc'tall    row = fromIntegral $ padding*(row+1) + width + lineHeight


--{- generate images, helper function
images     :: String -> [IO Image]
images home =
  [ do imageNewFromFile
  $ home <> prefix <> img <> suffix
  | img  <- map fst choices ]
--}

main :: IO ()
main  = do
  let
    len = fromIntegral $ length choices
    pad = fromIntegral padding :: Word32
  home <- getHomeDirectory
  user <- getEffectiveUserName

  Gtk.init Nothing

  win  <- windowNew WindowTypeToplevel
  grid <- gridNew
  -- generate an image, label, and button for each choice
  imgs <- sequence $ images home
  labs <- sequence $ take (length choices) $ repeat $ labelNew Nothing
  btns <- sequence $ take (length choices) $ repeat   buttonNew

  -- initialize the window
  set win
    [ #borderWidth          := pad
    , #title                := pack "Byebye"
    , #defaultWidth         := calc'wide len
    , #defaultHeight        := calc'tall $ fromIntegral 2
    , #resizable            := False
    , #windowPosition       := WindowPositionCenter
    , #decorated            := False
    ]

  -- initialize the grid
  set grid
    [ #columnSpacing        := padding
    , #rowSpacing           := padding
    , #columnHomogeneous    := True
    ]

  -- map over choices
  -- give each choice an index
  -- unpack choices and indices into arguments
  flip mapM ([0..] `zip` choices) \(i, (txt, act)) -> do
    -- define convenience variables
    let
      text   = pack $ "<b>"<>txt<>"</b>"
      label  = labs!!i
      button = btns!!i
      image  = imgs!!i
      i'     = fromIntegral i :: Int32
      ctxt   = "<b>confirm</b>"
    set label
      [ #label     := text
      , #useMarkup := True
      ]
    set button
      [ #relief    := ReliefStyleNone
--    , #image     := (Just image)
      , #hexpand   := False
      ]
    buttonSetImage   button $ Just image
    #attach grid button i' 0  1  1
    #attach grid label  i' 1  1  1
    on button #clicked $ do
      lbl <- get label #label
      -- if not confirm - early exit
      -- check if in confirm mode
      if confirm==False || unpack lbl == ctxt then do
        -- print:act:exit
        putStrLn $ "User chose: " <> txt
        act >> widgetDestroy win
      else pure ()
      -- recursive unbold function
      let
        norm (x:xs) = do
          set (labs!!x) [ #label := pack $ fst (choices!!x) ]
          norm $ xs
        norm _      = pure ()

      set label [ #label := pack ctxt ]
      norm $ filter (/= i) [0..length choices-1]

      #showAll win

  #add win grid

  win `onWidgetDestroy` mainQuit
  #showAll win
  Gtk.main
